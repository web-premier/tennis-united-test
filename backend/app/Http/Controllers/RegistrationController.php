<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class RegistrationController extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function __invoke(Request $request)
    {
        if(
            empty($request->input('name')) OR
            empty($request->input('email')) OR
            empty($request->input('password')) OR
            empty($request->input('password_confirmation'))
        ){
            return response()->json(['message'=>'Заполните все поля формы'], 422);
        }

        if(!filter_var($request->input('email'), FILTER_VALIDATE_EMAIL)){
            return response()->json(['message'=>'Введите корреткный e-mail'], 422);
        }

        if($request->input('password') !== $request->input('password_confirmation')){
            return response()->json(['message'=>'Пароли не совпадают'], 422);
        }

        $user = new User();
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->password = $request->input('password');

        $user->save();

        return response()->json([]);
    }
}
